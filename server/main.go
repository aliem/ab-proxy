package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	role := os.Getenv("ROLE")
	if len(role) == 0 {
		role = "master"
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Role: "+role)
	})

	fmt.Println("Listening to :3000 with role " + role)
	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		panic("HTTP Server Error: " + err.Error())
	}
}
